from django.contrib import admin

from BMS_App.models import *

class BackupAdmin(admin.ModelAdmin):
    exclude=("hash_id",)

admin.site.register(Backup, BackupAdmin)
admin.site.register(Client)
admin.site.register(Restriction)
admin.site.register(Contact)
admin.site.register(Response)
admin.site.register(Email)
admin.site.register(IgnoreDirectory)
