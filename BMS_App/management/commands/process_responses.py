from django.core.management.base import BaseCommand, CommandError
from BMS_App.models import Response,Backup
from WebHelpDeskAPILib import addNoteToTicket
from django.utils.timezone import localtime
import os
from BMS_Site import settings

class Command(BaseCommand): 
  help = "Processes any received responses from clients."
  
  def handle(self, *args, **options):
    responses = Response.objects.filter(addedToWHDTicket=False)
    for response in responses:
      created = localtime(response.created) 
      ticketText = "Client responded on: " + str(created) + "\nResponse: " + response.get_response_display() 
      if(response.response == "KP"):
        ticketText = ticketText + "\nJustification: " + response.justification

      success = addNoteToTicket(response.backup.ticket_number, ticketText)

      if(response.response == "DL"):
        processDeletableDirectory(response.backup)
      
      
      if(success[0]):
        response.addedToWHDTicket = True
        response.save()




def processDeletableDirectory(backup):
    backupPath = os.path.join(settings.BACKUP_DIRECTORY_PATH, backup.backup_location)
    if(os.path.exists(backupPath)):
        if('OK TO DELETE' not in backup.backup_location):
            # Rename the folder instead of actually deleting the subdirectory.
            newName = backup.backup_location + "- OK TO DELETE"
            os.rename(backupPath, os.path.join(settings.BACKUP_DIRECTORY_PATH, newName))
            backup.backup_location = newName
            backup.save()
    else:
        addNoteToTicket(backup.ticket_number, "Error - could not mark the directory as 'OK TO DELETE', the saved backup location does not exist:\nSaved Backup Location: " + str(backup.backup_location))  
