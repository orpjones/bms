from django.core.management.base import BaseCommand
from BMS_App.models import Backup, Contact, Restriction, Client, Email, IgnoreDirectory
import os
import re
from django.utils import timezone
from BMS_Site import settings
from django.db import IntegrityError
import subprocess
from subprocess import CalledProcessError
import logging

from WebHelpDeskAPILib import CreateTicketAction, AddNoteAction, UpdateStatusAction,\
    EmailNoteToClientAction, GetTicketStatusAction, GetClientDetailsAction, \
    TICKET_STATUS_TYPES

logger = logging.getLogger(__name__)


class BackupStats:
    """
    This class helps count the changes made when looping over the backups.
    """

    def __init__(self):
        self._folders_counter = 0             # Number of directories parsed
        self._new_backup_counter = 0          # Number of new backups
        self._in_progress_backup_counter = 0  # Number of in progress backups
        self._closed_backup_counter = 0       # Number of closed backups
        self._ignored_backup_counter = 0      # Number of directories we have ignored
        self._backups_created_counter = 0     # Number of backups created/changed to New
        self._in_progress_backups_delta = 0   # Number of backups changed from New to In Progress
        self._closed_backups_delta = 0        # Number of backups changed from In Progress to Closed
        self._new_client_counter = 0          # Number of clients created
        self._unexpected_errors_counter = 0   # Number of unknown problems we ran into while iterating over the backups
        self._in_progress_backup_with_restriction_counter = 0

    def processed_folder(self, num=1):
        self._folders_counter += num

    def processed_new_backup(self, num=1):
        self._new_backup_counter += num

    def processed_in_progress_backup(self, num=1):
        self._in_progress_backup_counter += num

    def processed_closed_backup(self, num=1):
        self._closed_backup_counter += num

    def processed_ignored_backup(self, num=1):
        self._ignored_backup_counter += num

    def created_backup(self, num=1):
        self._backups_created_counter += num

    def changed_to_in_progress(self, num=1):
        self._in_progress_backups_delta += num

    def closed_backup(self, num=1):
        self._closed_backups_delta += num

    def created_client(self, num=1):
        self._new_client_counter += num

    def unexpected_error_occurred(self, num=1):
        self._unexpected_errors_counter += num

    def processed_in_progress_backup_with_restriction(self, num=1):
        self._in_progress_backup_with_restriction_counter += num

    def print_stats(self):
        stats_string = """Stats:
        t{0} total directories processed
        t{1} directories ignored
        \t{2} New Backups
        \t{3} INP Backups
        \t{4} INP Backups with restrictions"
        \t{5} CLO Backups
        \t{6} Clients Created
        \t{7} Backups Created
        \t{8} Backups changed from NEW to INP
        \t{9} Backups changed from INP to CLO
        \t{10} unexpected errors occurred."""\
            .format(str(self._folders_counter),
                    str(self._ignored_backup_counter),
                    str(self._new_backup_counter),
                    str(self._in_progress_backup_counter),
                    str(self._in_progress_backup_with_restriction_counter),
                    str(self._closed_backup_counter),
                    str(self._new_client_counter),
                    str(self._backups_created_counter),
                    str(self._in_progress_backups_delta),
                    str(self._closed_backups_delta),
                    str(self._unexpected_errors_counter))

        logger.info(stats_string)

MISMATCHED_BACKUP_LOCATION_ERROR_STRING = """Backup Management System Error:

Error while parsing the directory:
    '{0}'


The best guess for this error is that:
    A) Someone changed either:
            i. the folder name on the backup server or
            ii. the saved location for this backup on the web console
    B) Someone created two backups with the same ticket number.


To fix this error:
    Case A:
        1) Log on to the web console and open this backup.
        2) Make sure the backup location on the web console matches the actual"
           folder name.
        3) Change one or the other as necessary.

    Case B has two solutions (I and II):
        I) Move the first and second folder into a common directory. For example:
              John Doe - 12345
              John Doe 2 - 12345
           Would become:
              John Doe - 12345
                  John Doe
                  John Doe 2

        II) Create a second ticket for the second folder. For example:
                John Doe - 12345
                John Doe 2 - 12345
            Would become:
                John Doe - 12345
                John Doe - 99999

The saved folder name on web console is: {1}
The folder name on backup server is: {2}"""

INTEGRITY_ERROR_STRING = """Backup Management System Error:

Error while trying to create a new backup.

We were parsing the directory:
    '{0}'

The best guess for the error is that there are multiple backups with the same ticket number.

To fix this either:
    A) Move the first and second folder into a common directory. For example:
            John Doe - 12345
            John Doe 2 - 12345
        Would become:
            John Doe - 12345
                John Doe
                John Doe 2

    B) Create a second ticket for the second folder. For example:
            John Doe - 12345
            John Doe 2 - 12345
        Would become:
            John Doe - 12345
            John Doe - 99999

Full Error Message:
    {1}"""

INVALID_DIRECTORY_SYNTAX_EXCEPTION_ERROR_STRING = """Backup Management System Error

Invalid Directory Syntax for directory:
    '{0}'

The correct format should be the following:
    FirstName LastName - TicketNumber - ...

To correct this, simply rename the directory to use the correct format."""

CLIENT_NAME_MISMATCH_EXCEPTION_ERROR_STRING = """Backup Management System Error

The client's name in the ticket and the client's name on the backup do not match.

This can be caused by a mistyped ticket number (Case A),
or a spelling error in the directory on the backup server (Case B).

Steps to Correct Case A:
    1) Rename the folder on the backup server to include the correct ticket number.
    2) Go into the web interface, find the backup, and correct the ticket number.

Steps to Correct Case B:
    1) Rename the folder on the backup server to the correct name/ticket number.
    2) Go into the web interface, find the client, and correct their name.
    3) Go into the web interface, find the backup, and correct the backup location.

The client name on the backup server is: {0}
The client name on the WHD ticket is: {1}"""


class InvalidDirectorySyntaxException(Exception):

    def __init__(self, invalid_directory_name):
        super(InvalidDirectorySyntaxException, self).__init__()
        self.invalid_directory_name = invalid_directory_name

    def __str__(self):
        return INVALID_DIRECTORY_SYNTAX_EXCEPTION_ERROR_STRING.format(self.invalid_directory_name)


class ClientNameMismatchException(Exception):

    def __init__(self, web_help_desk_client_name, backup_client_name):
        super(ClientNameMismatchException, self).__init__()
        self.web_help_desk_client_name = web_help_desk_client_name
        self.backup_client_name = backup_client_name

    def __str__(self):
        return CLIENT_NAME_MISMATCH_EXCEPTION_ERROR_STRING.format(self.backup_client_name,
                                                                  self.web_help_desk_client_name)


class Command(BaseCommand):
    help = "Iterates through " + settings.BACKUP_DIRECTORY_PATH + " and updates the backups."

    TEXT_BACKUP_STATUS_CHANGE_TO_IN_PROGRESS = "Backup age is older than 2 weeks. Marking as In Progress."

    def __init__(self):
        super(Command, self).__init__()
        self.stats = BackupStats()
        self.send_to_client = True
        self.push_to_web_help_desk = True
        self.create_tickets_for_errors = True
        self.save_to_db = True
        self.rename_error_directories = True
        self.rename_deletable_directories = True
        self.should_get_folder_size = True

        file_log_handler = logging.FileHandler('/home/stsadmin/update_backups_log.txt')
        file_log_formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        file_log_handler.setFormatter(file_log_formatter)

        console_log_handler = logging.StreamHandler()
        console_log_formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        console_log_handler.setFormatter(console_log_formatter)

        logger.addHandler(file_log_handler)
        logger.addHandler(console_log_handler)
        logger.setLevel(logging.WARNING)

    def handle(self, *args, **options):
        logging.info("Processing directory: " + settings.BACKUP_DIRECTORY_PATH)

        # Get all subdirectories
        directory_names = os.walk(settings.BACKUP_DIRECTORY_PATH).next()[1]
        for folder_name in directory_names:
            folder_name = unicode(folder_name, 'utf-8')
            folder_name = folder_name.strip()
            self.stats.processed_folder()

            # White list of folders
            if Command.should_ignore_directory(folder_name):
                logger.info("Ignoring directory: {0}".format(folder_name))
                self.stats.processed_ignored_backup()
                continue

            try:
                self.process_directory(folder_name)
            except Exception as e:
                logger.critical("Unexpected error occurred while processing folder '{0}':\n\t{1}"
                                .format(folder_name, unicode(e.message)))
                self.stats.unexpected_error_occurred()
            continue

        self.stats.print_stats()

    def process_directory(self, folder_name):
        logger.debug("Processing directory: {0}".format(folder_name))
        # Use regular expression to split directory name
        # Accepts whitespaces, dashes, double dashes, etc.
        # Underscores won't work
        directory_information = re.findall(r"[\w]+", folder_name)
        logger.debug("Got directory information: {0}".format(unicode(directory_information)))

        # Get the ticket number and client name
        try:
            ticket_number_index = Command.parse_ticket_number_index(directory_information, folder_name)
        except InvalidDirectorySyntaxException as exception:
            # Parsing ticket number index failed, so report it by creating a ticket.
            logger.warning("Invalid Directory Syntax Exception Occurred for: {0}".format(folder_name))
            if self.create_tickets_for_errors:
                CreateTicketAction("Backup Management System Error", str(exception)).perform_action()
                logger.info("Created ticket for Invalid Directory Syntax Exception.")
            return

        ticket_number = directory_information[ticket_number_index]
        logger.debug("Ticket number: {0}".format(ticket_number))

        client_name = Command.get_client_name_from_directory_information(directory_information, ticket_number_index)
        logger.debug("Client name: {0}".format(client_name))

        folder_path = os.path.join(settings.BACKUP_DIRECTORY_PATH, folder_name)
        logger.debug("Folder Path: {0}".format(folder_path))

        folder_size = self.get_folder_size(folder_path)
        logger.debug("Folder Size: {0}".format(unicode(folder_size)))

        current_backup = self.get_backup(ticket_number, client_name, folder_name, folder_size)
        if current_backup is None:
            return

        # If the backup location has changed, and we're running in release
        # then that's a problem because we don't know if this is the same
        # backup or not. We need to create a ticket for it.
        if current_backup.backup_location != folder_name:
            logger.warning("Backup location has changed for backup: {0}".format(current_backup))

            if self.create_tickets_for_errors:
                CreateTicketAction(
                    "Backup Management System Error",
                    MISMATCHED_BACKUP_LOCATION_ERROR_STRING.format(folder_name,
                                                                   current_backup.backup_location,
                                                                   folder_name)).perform_action()
                logger.info("Created ticket for mismatched backup location.")

            self.process_error_directory(current_backup)
            return

        self.process_backup(current_backup)

        # Just to keep things updated
        # The != 0 is because get_folder_size may fail, and if it does then folder_size is set to 0
        # in that case we just want to keep the old folder size (i.e. not execute the below code)
        if folder_size != 0:
            logger.info("Updating folder size to {0} from {1}".format(unicode(folder_size),
                                                                      unicode(current_backup.size_in_megabytes)))
            current_backup.size_in_megabytes = folder_size
            if self.save_to_db:
                current_backup.save()
        logger.debug("Processed directory: {0}".format(folder_name))

    def process_backup(self, backup):
        logger.debug("Processing backup.")
        if backup.status == 'NEW':
            self.process_new_backup(backup)

        elif backup.status == 'INP':
            self.process_in_progress_backup(backup)

        elif backup.status == 'CLO':
            self.process_closed_backup(backup)
        logger.debug("Processed backup.")

    def process_new_backup(self, backup):
        logger.debug("Processing New backup.")

        # Check to see if this "NEW" backup should be changed to "IN PROGRESS"
        days_elapsed = (timezone.now() - backup.created).days
        logger.debug("Days Elapsed for backup: {0}".format(unicode(days_elapsed)))
        if days_elapsed > settings.BACKUP_POLICY_DAYS:
            self.move_to_in_progress(backup)
        else:
            self.stats.processed_new_backup()
        logger.debug("Processed New backup.")

    def process_in_progress_backup(self, backup):
        logger.debug("Processing In Progress Backup")

        # Check if there are any restrictions on the backup. If so, we don't process it.
        if not Command.check_for_restrictions(backup):
            # 6/3/13: Currently set to 35 days before marking as deletable.
            # Note that this is ignored if there is a restriction in place.
            days_elapsed = (timezone.now() - backup.created).days

            # Update the WHD ticket by adding a note and emailing it to the client.
            add_note_action = AddNoteAction(backup.ticket_number,
                                            Command.get_email_text(backup,
                                                                   days_elapsed > settings.MAX_DAYS_ELAPSED),
                                            is_hidden=(not self.send_to_client))
            if self.push_to_web_help_desk:
                add_note_action.perform_action()
                logger.info("Added note to ticket for In Progress backup.")
                if self.send_to_client:
                    EmailNoteToClientAction(backup.ticket_number, add_note_action.get_note_id()).perform_action()
                    logger.info("Sent note to client.")
                    contact = Contact(backup=backup)
                    if self.save_to_db:
                        contact.save()

            # Close the backup if its older than the maximum allowed.
            if days_elapsed > settings.MAX_DAYS_ELAPSED:
                self.close_backup(backup)
            else:
                self.stats.processed_in_progress_backup()
        else:
            self.stats.processed_in_progress_backup_with_restriction()
        self.ensure_correct_ticket_status_for_in_progress_backup(backup)
        logger.debug("Processed In Progress Backup")

    def process_closed_backup(self, backup):
        logger.debug("Processing Closed backup")
        self.stats.processed_closed_backup()
        self.process_deletable_directory(backup)
        logger.debug("Processed Closed backup")

    def process_deletable_directory(self, backup):
        logger.debug("Processing deletable directory")
        if self.rename_deletable_directories and 'OK TO DELETE' not in backup.backup_location:
            # Rename the folder instead of actually deleting the subdirectory.
            new_name = backup.backup_location + "- OK TO DELETE"
            os.rename(os.path.join(settings.BACKUP_DIRECTORY_PATH, backup.backup_location),
                      os.path.join(settings.BACKUP_DIRECTORY_PATH, new_name))
            backup.backup_location = new_name
            logger.info("Renamed folder '{0}' to '{1}'".format(backup.backup_location, new_name))
            if self.save_to_db:
                backup.save()
        logger.debug("Processed deletable directory")

    def process_error_directory(self, backup):
        logger.debug("Processing error directory")
        if self.rename_error_directories and 'ERROR IGNORED' not in backup.backup_location:
            # Rename the folder instead of actually deleting the subdirectory.
            new_name = backup.backup_location + " - ERROR IGNORED"
            os.rename(os.path.join(settings.BACKUP_DIRECTORY_PATH, backup.backup_location),
                      os.path.join(settings.BACKUP_DIRECTORY_PATH, new_name))
            backup.backup_location = new_name
            logger.info("Renamed folder '{0}' to '{1}'".format(backup.backup_location, new_name))
            if self.save_to_db:
                backup.save()
        logger.debug("Processed error directory")

    def move_to_in_progress(self, backup):
        logger.debug("Changing backup status to In Progress.")
        backup.status = 'INP'

        if self.save_to_db:
            backup.save()

        # Log and record stats.
        self.stats.changed_to_in_progress()

        # Add a note to the WHD Ticket saying we've changed the backup status
        # from "NEW" to "IN PROGRESS".
        if self.push_to_web_help_desk:
            AddNoteAction(backup.ticket_number,
                          self.TEXT_BACKUP_STATUS_CHANGE_TO_IN_PROGRESS).perform_action()
            logger.info("Added note to ticket informing of status change to In Progress.")
            self.ensure_correct_ticket_status_for_in_progress_backup(backup)

        logger.debug("Changed backup status to In Progress.")

    def close_backup(self, backup):
        logger.debug("Closing backup.")
        backup.status = 'CLO'

        if self.save_to_db:
            backup.save()

        self.stats.closed_backup()

        if self.push_to_web_help_desk:
            AddNoteAction(backup.ticket_number, "Backup age is older than the maximum allowed. Closing backup.")\
                .perform_action()
            logger.info("Added note to WHD ticket informing the backup was older than maximum.")

        self.process_deletable_directory(backup)
        logger.debug("Closed backup.")

    def get_backup(self, ticket_number, client_name, folder_name, folder_size):
        logger.debug("Retrieving backup.")
        # See if client and backup exist, otherwise create them.
        try:
            current_backup = Backup.objects.get(ticket_number=ticket_number)
            logger.debug("Found an existing backup for ticket number {0} and folder: {1}"
                         .format(unicode(ticket_number), folder_name))
        except Backup.DoesNotExist:
            logger.debug("Could not find an existing backup for ticket number {0} and folder {1}"
                         .format(unicode(ticket_number), folder_name))

            # Get/create a client. Then create a new backup object.
            current_client = self.get_client(client_name)
            logger.debug("Got client for {0}".format(client_name))
            try:
                self.verify_client_name(client_name, ticket_number)
            except ClientNameMismatchException as mismatch:
                logger.warning("Client name mismatch occurred for client name: {0}".format(client_name))
                if self.create_tickets_for_errors:
                    CreateTicketAction("Backup Management System Error", str(mismatch)).perform_action()
                    logger.debug("Filed ticket for client name mismatch exception")
                return

            logger.debug("Verified client name")
            try:
                # Create a new backup
                logger.info("Creating new backup for {0} and folder: {1}".format(unicode(ticket_number), folder_name))
                current_backup = Backup.objects.create(client=current_client,
                                                       status='NEW',
                                                       backup_location=folder_name,
                                                       size_in_megabytes=folder_size,
                                                       ticket_number=ticket_number,
                                                       created=timezone.now())
                self.stats.created_backup()
            except IntegrityError as e:
                # Notify on some type of error.
                logger.warning("Integrity Error while creating new backup for ticket number: {0} and folder: {1}"
                               .format(unicode(ticket_number), folder_name))

                if self.create_tickets_for_errors:
                    CreateTicketAction("Backup Management System Error",
                                       INTEGRITY_ERROR_STRING.format(folder_name, e.message)).perform_action()
                    logger.info("Created ticket for integrity error.")
                return
        logger.debug("Retrieved backup.")
        return current_backup

    def get_client(self, client_name):
        logger.debug("Retrieving client for client name: {0}.".format(client_name))
        try:
            client = Client.objects.get(name=client_name)
            logger.debug("Found an existing client for {0}.".format(client_name))
        except Client.DoesNotExist:
            logger.debug("Could not find an existing client for {0}." + client_name)
            # Create new client
            client = Client.objects.create(name=client_name)
            if self.save_to_db:
                client.save()
            self.stats.created_client()
        logger.debug("Retrieved client.")
        return client

    def get_folder_size(self, folder):
        logger.debug("Getting folder size for '{0}'".format(folder))
        # This function will launch a subprocess and run the shell command du (disk usage)
        # and ask the operating system to calculate the folder size so that we don't have to
        # (it was causing all sorts of problems)

        size_in_megabytes = 0
        if self.should_get_folder_size:
            try:
                folder_path = os.path.join(settings.BACKUP_DIRECTORY_PATH, folder)
                s = subprocess.check_output(["du", "-sk", folder_path], stderr=subprocess.STDOUT)
                s = unicode(s, 'utf-8')
                s = s.split()
                size_in_megabytes = int(s[0])/1024.0
            except CalledProcessError:
                logger.critical("An error occurred getting the size of '{0}'".format(folder))
        logger.debug("Got folder size of '{0}' for folder '{1}'".format(unicode(size_in_megabytes), folder))
        return size_in_megabytes

    def ensure_correct_ticket_status_for_in_progress_backup(self, backup):
        # Update the ticket to HOLD if its CLOSED
        logger.debug("Checking to see if we need to change WHD ticket status.")
        if self.push_to_web_help_desk:
            get_ticket_status_action = GetTicketStatusAction(backup.ticket_number)
            if get_ticket_status_action.perform_action():
                logger.debug("Ticket status is: {0}".format(get_ticket_status_action.get_status()))
                if get_ticket_status_action.get_status() == TICKET_STATUS_TYPES.CLOSED:
                    UpdateStatusAction(backup.ticket_number,
                                       status_type_id=TICKET_STATUS_TYPES.HOLD).perform_action()
                    logger.info("Changed WHD status from CLOSED to HOLD.")
        logger.debug("Finished checking to see if we need to change WHD ticket status.")

    @staticmethod
    def should_ignore_directory(folder_name):
        # TODO - logging
        q = IgnoreDirectory.objects.filter(folderName=folder_name)
        return q.count() > 0

    @staticmethod
    def get_email_text(backup, last_contact=False):
        # TODO - logging
        """
        This method will get the email text from the Email model. It currently supports only
        two emails. The first email we send them (and all following until the last one) will
        email the client the link to a response form.

        If it is the last time we will contact them, then we simply say we are deleting their
        data. No response form.
        """
        site_url = settings.RESPONSE_FORM_URL
        if last_contact:
            email_text = Email.objects.all().filter(order=1)[0].text
            email_text = email_text.format(customerName=backup.client.name)
        else:
            email_text = Email.objects.all().filter(order=0)[0].text
            email_text = email_text.format(customerName=backup.client.name, siteURL=site_url, backupHash=backup.hash_id)
        return email_text

    @staticmethod
    def check_for_restrictions(backup):
        logger.debug("Checking for restrictions for {0}".format(backup))
        restrictions_list = Restriction.objects.all().filter(backup=backup)
        restrictions_apply = False
        for restriction in restrictions_list:
            difference_days = (restriction.restrictContactsUntil - timezone.now().date()).days
            if difference_days > 0:
                restrictions_apply = True
                break
        logger.debug("Finished checking for restrictions for {0}. Restrictions apply: {1}".format(backup,
                                                                                                  restrictions_apply))
        return restrictions_apply

    @staticmethod
    def parse_ticket_number_index(directory_information, folder_name):
        logger.debug("Parsing ticket number index.")
        ticket_number_index = 0
        for i in range(len(directory_information)):
            info = unicode(directory_information[i])
            # Currently, ticket number is at 80,000
            if info.isnumeric() and len(info) > 4:
                ticket_number_index = i
                break

        if ticket_number_index == 0:
            raise InvalidDirectorySyntaxException(folder_name)
        logger.debug("Finished parsing ticket number index.")
        return ticket_number_index

    @staticmethod
    def verify_client_name(directory_client_name, ticket_number):
        # TODO - logging
        ticket_details_action = GetClientDetailsAction(ticket_number)
        ticket_details_action.perform_action()

        client_name = ticket_details_action.get_client_name()
        directory_client_name = directory_client_name.strip()
        client_name = client_name.strip()

        if not directory_client_name in client_name:
            raise ClientNameMismatchException(directory_client_name, client_name)

    @staticmethod
    def get_client_name_from_directory_information(directory_information, ticket_number_index):
        # TODO - logging
        # Parses out the client name from the directory information
        # directory_information is usually in the follow formats:
        #   John Smith - 123456 - ...
        #   Walter Bruce Willis - 123456 - ...
        full_client_name = directory_information[0:ticket_number_index]

        # full_client_name would now contain:
        #   John Smith
        #   Walter Bruce Willis
        # But, as a list. Join everything together and remove whitespace.
        client_name = ''.join(unicode(word) + " " for word in full_client_name)
        client_name = client_name .strip()
        return client_name