# Create your views here.
from django.views.generic.edit import UpdateView, CreateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, get_object_or_404, get_list_or_404, redirect
from django.contrib.auth.decorators import login_required
from BMS_App.models import Client, Backup, Contact, Response, Restriction
from django.contrib import messages
from django.views.generic import ListView
from django.db import models
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import datetime
from django.utils import timezone
from django.http import Http404
import re
from django.db.models import Sum, Avg, Count
from django import forms
from django.forms import ModelForm

# Number of objects that should be displayed per page when rendered in the index view
NUM_OBJ_PER_PAGE_INDEX_VIEWS = 25

# Helper method to reduce code
def getPage(paginator, page):
  obj_list = []
  try:
    obj_list = paginator.page(page)
  except PageNotAnInteger:
    # If page is not an integer, deliver first page.
    obj_list = paginator.page(1)
  except EmptyPage:
    # If page is out of range (e.g. 9999), deliver last page of results.
    obj_list = paginator.page(paginator.num_pages)
  return obj_list


##### Home Page #################

def home(request):
  # get values and convert to GB
  # Current backups (New and In progress)
  AllINP = Backup.objects.all().filter(status='INP').aggregate(Sum('size_in_megabytes'), Avg('size_in_megabytes'), Count('id'))
  AllNEW = Backup.objects.all().filter(status='NEW').aggregate(Sum('size_in_megabytes'), Avg('size_in_megabytes'), Count('id'))
  
  sumINP = AllINP['size_in_megabytes__sum']  
  sumNEW = AllNEW['size_in_megabytes__sum']  
  
  if(sumINP == None): sumINP = 0
  if(sumNEW == None): sumNEW = 0

  totalINPSpaceUsage = int(sumINP)/1024
  totalNEWSpaceUsage = int(sumNEW)/1024

  numINP = AllINP['id__count']
  numNEW = AllNEW['id__count']
  if(numINP == None): numINP = 0
  if(numNEW == None): numNew = 0
  numberOfCurrentBackups = int(numINP) + int(numNEW)
 
  avgINP = AllINP['size_in_megabytes__avg']
  avgNEW = AllNEW['size_in_megabytes__avg']

  if(avgINP == None): avgINP = 0
  if(avgNEW == None): avgNEW = 0

  avgCurrentBackupSize = (int(avgINP) + int(avgNEW))/(1024 * 2)
  totalCurrentSpaceUsage = totalINPSpaceUsage + totalNEWSpaceUsage
  totalCurrentSpaceUsagePercentage = (float(totalCurrentSpaceUsage)/ (6*1024)) * 100 # x / 6 TB

  ## ALL Backups from all time
  allBackups = Backup.objects.all().aggregate(Sum('size_in_megabytes'), Avg('size_in_megabytes'), Count('id'))
  sizeSum = allBackups['size_in_megabytes__sum']
  sizeAvg = allBackups['size_in_megabytes__avg']
  count   = allBackups['id__count']

  if(sizeSum == None): sizeSum = 0
  if(sizeAvg == None): sizeAvg = 0
  if(count   == None): count   = 0

  totalSpaceUsage = int(sizeSum)/1024
  avgBackupSize   = int(sizeAvg)/1024
  numberOfBackups = count

  temp = totalCurrentSpaceUsagePercentage
  totalCurrentSpaceUsagePercentage = '{:.2%}'.format(totalCurrentSpaceUsagePercentage/100)
  if(temp - 50 > 0):
    totalCurrentSpaceUsagePercentageSuccess = 50
    temp = temp - 50
  else:
    totalCurrentSpaceUsagePercentageSuccess = temp
    temp = 0

  if(temp - 40 > 0):
    totalCurrentSpaceUsagePercentageWarning = 40
    temp = temp - 40
  else:
    totalCurrentSpaceUsagePercentageWarning = temp
    temp = 0

  if(temp - 10 > 0):
    totalCurrentSpaceUsagePercentageDanger = 10
    temp = temp - 10
  else:
    totalCurrentSpaceUsagePercentageDanger = temp 
    temp = 0
  
  return render(request, 'home.html', 
            { 
              'totalCurrentSpaceUsage' : totalCurrentSpaceUsage,
              'avgCurrentBackupSize' : avgCurrentBackupSize,
              'totalCurrentSpaceUsagePercentage' : totalCurrentSpaceUsagePercentage,
              'totalCurrentSpaceUsagePercentageSuccess' : totalCurrentSpaceUsagePercentageSuccess,
              'totalCurrentSpaceUsagePercentageWarning' : totalCurrentSpaceUsagePercentageWarning,
              'totalCurrentSpaceUsagePercentageDanger'  : totalCurrentSpaceUsagePercentageDanger,
              'numberOfCurrentBackups' : numberOfCurrentBackups,
              'totalSpaceUsage' : totalSpaceUsage,
              'avgBackupSize' : avgBackupSize,
              'numberOfBackups' : numberOfBackups,
            })

##### Clients ###################

@login_required
def client_view(request, pk):
  client = get_object_or_404(Client, pk=pk)
  try:
    backups = Backup.objects.filter(client=client)
  except Backup.DoesNotExist:
    backups = []
  return render(request, 'BMS_App/client_view.html', {'client' : client, 'backups' : backups}) 

@login_required
def client_index(request):
  clients_list = Client.objects.all().order_by('-created')
  paginator = Paginator(clients_list, NUM_OBJ_PER_PAGE_INDEX_VIEWS)
  page = request.GET.get('page')
  clients_list = getPage(paginator, page)

  return render(request, 'BMS_App/client_index.html', {'clients_list' : clients_list, 'paginator' : paginator})

@login_required
def client_search(request):
  if request.method == 'POST': # If the form has been submitted...
    form = ClientSearchForm(request.POST) # A form bound to the POST data
    if form.is_valid(): # All validation rules pass
      name = form.cleaned_data['name']
      clients_list = Client.objects.all().filter(name__icontains=name)
      return render(request, 'BMS_App/client_search_results.html', {'clients_list' : clients_list})
  else:
    form = ClientSearchForm() # An unbound form

    return render(request, 'BMS_App/client_search.html', {
        'form': form,
      })


##### Backups ###################
@login_required
def backup_view(request, pk):
  try:
    backup = Backup.objects.get(ticket_number=pk)
  except Backup.DoesNotExist:
    raise Http404

  try:
    contacts = Contact.objects.filter(backup=backup).order_by('-created')[:10]
  except Contact.DoesNotExist:
    contacts = []

  try:
    restrictions = Restriction.objects.filter(backup=backup).order_by('-created')[:10]
  except Restriction.DoesNotExist:
    restrictions = []
  
  try:
    responses = Response.objects.filter(backup=backup).order_by('-created')[:10]
  except Response.DoesNotExist:
    responses = []

  return render(request, 'BMS_App/backup_view.html', {'backup' : backup, 'contacts' : contacts, 'restrictions' : restrictions, 'responses' : responses}) 

@login_required
def backup_index(request):
  backups_list = Backup.objects.all().order_by('-created')
  paginator = Paginator(backups_list, NUM_OBJ_PER_PAGE_INDEX_VIEWS)
  page = request.GET.get('page')
  backups_list = getPage(paginator, page)

  return render(request, 'BMS_App/backup_index.html', {'page_obj' : backups_list, 'paginator' : paginator})

class BackupEditForm(ModelForm):
  class Meta:
    model = Backup
    fields = ["client", "ticket_number", "backup_location", "size_in_megabytes", "status"]


class BackupIndexList(ListView):
  template_name = 'BMS_App/backup_index.html'
  model = Backup
  paginate_by = NUM_OBJ_PER_PAGE_INDEX_VIEWS

  def get_queryset(self):
    self.status = self.args[0]
    return Backup.objects.filter(status=self.args[0]).order_by('created')

  def get_context_data(self, **kwargs):
    # Call the base implementation first to get a context
    context = super(BackupIndexList, self).get_context_data(**kwargs)
    backup = Backup.objects.filter(status=self.status)

    if(len(backup) == 0):
      backup = []
      context['status'] = self.status
    else:
      backup = backup[0]
      context['status'] = backup.get_status_display()

    return context


##### Contacts ##################

@login_required
def contact_view(request, pk):
  contact = get_object_or_404(Contact, pk=pk)
  return render(request, 'BMS_App/contact_view.html', {'contact' : contact}) 

@login_required
def contact_index(request):
  contact_list = Contact.objects.all().order_by('-created')
  paginator = Paginator(contact_list, NUM_OBJ_PER_PAGE_INDEX_VIEWS)
  page = request.GET.get('page')
  contact_list = getPage(paginator, page)

  return render(request, 'BMS_App/contact_index.html', {'page_obj' : contact_list, 'paginator' : paginator})

##### Responses #################

@login_required
def response_view(request, pk):
  response = get_object_or_404(Response, pk=pk)
  return render(request, 'BMS_App/response_view.html', {'response' : response}) 

@login_required
def response_index(request):
  response_list = Response.objects.all().order_by('-created')
  paginator = Paginator(response_list, NUM_OBJ_PER_PAGE_INDEX_VIEWS)
  page = request.GET.get('page')
  response_list = getPage(paginator, page)
  
  return render(request, 'BMS_App/response_index.html', {'page_obj' : response_list, 'paginator' : paginator}) 

##### Restrictions ##############

@login_required
def restriction_view(request, pk):
  restriction = get_object_or_404(Restriction, pk=pk)
  return render(request, 'BMS_App/restriction_view.html', {'restriction' : restriction}) 

@login_required
def restriction_delete(request, pk):
  restriction = get_object_or_404(Restriction, pk=pk)
  backup = restriction.backup
  restriction.delete()
  return redirect(backup) 


@login_required
def restriction_create(request, pk):
  b = get_object_or_404(Backup, pk=pk)
  
  if request.method == 'POST': # If the form has been submitted...
    form = RestrictionForm(request.POST) 
    
    # A form bound to the POST data
    if form.is_valid(): # All validation rules pass
      restrictContactsUntil = form.cleaned_data['restrictContactsUntil']
      restriction = Restriction(restrictContactsUntil = restrictContactsUntil, backup=b)
      restriction.save()
      messages.success(request, "Restriction created successfully") 
      return redirect(b) # Redirect after POST
  else:
    form = RestrictionForm() # An unbound form
  
  return render(request, 'BMS_App/restriction_create.html', { 'form': form, 'backup' : b })
  

class RestrictionForm(ModelForm):
  class Meta:
    model = Restriction
    fields = ['restrictContactsUntil']



class RestrictionCreate(CreateView):
  """
  Uses a ModelForm (a form generated from a model) to allow users to create a 
  restriction. Pass in arguments via URLConf (urls.py) to
  set the model, template, and success message. Uses a Message Mixin to validate the
  form and show a message on success.
  """
  model  = Restriction
  fields = ['restrictContactsUntil']


##### MessageMixin ##############

class MessageMixin(object):
  """
  Make it easy to display notification messages when using Class Based Views.
  """
  success_message=''
  def form_valid(self, form):
    messages.success(self.request, self.success_message) 
    return super(MessageMixin, self).form_valid(form)


##### Custom Views ##############

class CustomUpdateView(MessageMixin, UpdateView):
  """
  Uses a ModelForm (a form generated from a model) to allow users to update a generic
  model without additional configuration. Pass in arguments via URLConf (urls.py) to
  set the model, template, and success message. Uses a Message Mixin to validate the
  form and show a message on success.
  """
  success_message = 'Updated successfully!'


class CustomCreateView(MessageMixin, CreateView):
  """
  Uses a ModelForm (a form generated from a model) to allow users to create a generic
  model without additional configuration. Pass in arguments via URLConf (urls.py) to
  set the model, template, and success message. Uses a Message Mixin to validate the
  form and show a message on success.
  """
  success_message = 'Created successfully!'


class RestrictionCreateView(MessageMixin, CreateView):
  """
  Uses a ModelForm (a form generated from a model) to allow users to update a generic
  model without additional configuration. Pass in arguments via URLConf (urls.py) to
  set the model, template, and success message. Uses a Message Mixin to validate the
  form.
  """
  success_message = 'Created successfully!'
  model = Restriction

  def form_valid(self, form):
    form.instance.backup = self.backup
    return super(RestrictionCreateView, self).form_valid(form)

class GenericCreatedTimestampView(ListView):
  """
  Displays a list of items based on url parameters and the date those
  items were created. 
   
  URL Parameters
  xH = Last x hours
  xW = Last x Weeks
  xM = Last x Months
  xY = Last x Years
  """

  template_name = 'BMS_App/contact_index.html'
  model = Contact
  paginate_by = NUM_OBJ_PER_PAGE_INDEX_VIEWS

  def get_queryset(self): 
    startDate = timezone.now()
    params = str(self.args[0])
    args = re.split(r'(\d+[HDWMY])', params)
    
    for arg in args:
      if "H" in arg:
        indexOfH = arg.find("H")
        if indexOfH != -1:
          number = int(arg[:indexOfH]) 
          startDate = startDate - datetime.timedelta(hours=number)
      elif "D" in arg:
        indexOfD = arg.find("D")
        if indexOfD != -1:
          number = int(arg[:indexOfD]) + 1
          startDate = startDate - datetime.timedelta(days=number)
      elif "W" in arg:
        indexOfW = arg.find("W")
        if indexOfW != -1:
          number = int(arg[:indexOfW])
          startDate = startDate - datetime.timedelta(weeks=number)
      elif "M" in arg:
        indexOfM = arg.find("M")
        if indexOfM != -1:
          number = int(arg[:indexOfM])
          startDate = startDate - datetime.timedelta(weeks=4*number)
      elif "Y" in arg:
        indexOfY = arg.find("Y")
        if indexOfY != -1:
          number = int(arg[:indexOfY])
          startDate = startDate - datetime.timedelta(weeks=52*number)
    self.startDate = startDate
    return self.model.objects.all().filter(created__range=[startDate, timezone.now()]).order_by('-created')

  def get_context_data(self, **kwargs):
    # Call the base implementation first to get a context
    context = super(GenericCreatedTimestampView, self).get_context_data(**kwargs)
    #backup = Backup.objects.filter(status=self.status)[0]
    context['startDate'] = self.startDate
    return context

def custom_range(request):
  print "HERE"
  print request.POST['model']
  contact = Contact.objects.all()[0]
  return redirect(contact)

##### Form ######################

class ClientSearchForm(forms.Form):
  name = forms.CharField(max_length=60)

