from django.db import models
from django.forms import ModelForm
from django.core.urlresolvers import reverse
import os
from binascii import hexlify
import uuid

def _createId():
  return uuid.uuid1().hex #uuid.uuid4()#hexlify(os.urandom(16))

class Client(models.Model):
  name = models.CharField(max_length=60, db_index=True)
  created = models.DateTimeField(auto_now=False, auto_now_add=True)
  updated = models.DateTimeField(auto_now=True, auto_now_add=False)

  def __unicode__(self):
    return self.name
  
  def get_absolute_url(self):
    return reverse('BMS_App.views.client_view', kwargs={'pk' : self.pk})

class Backup(models.Model):
  client = models.ForeignKey(Client)
  ticket_number = models.PositiveIntegerField(unique=True, db_index=True)
  backup_location = models.CharField(max_length=1024)
  size_in_megabytes = models.FloatField()
  # Create a random hash
  hash_id = models.CharField(max_length=32, unique=True, db_index=True, default=_createId)

  NEW = u'NEW'
  IN_PROGRESS = u'INP'
  CLOSED = u'CLO'
  ERROR = u'ERR'

  STATUS_CHOICES = (
      (NEW, u'New'),
      (IN_PROGRESS, u'In Progress'),
      (CLOSED, u'Closed'),
      (ERROR, u'Error'),
  )

  status = models.CharField(max_length=3, choices=STATUS_CHOICES, default=NEW)
  created = models.DateTimeField(auto_now=False, auto_now_add=True)
  updated = models.DateTimeField(auto_now=True, auto_now_add=False)

  def __unicode__(self):
    return self.status + " " + self.backup_location

  def get_absolute_url(self):
    return reverse('BMS_App.views.backup_view', kwargs={'pk' : self.ticket_number})

class Contact(models.Model):
  backup = models.ForeignKey(Backup, db_index=True)
  created = models.DateTimeField(auto_now=False, auto_now_add=True)

  def __unicode__(self):
    return unicode(self.created)

class Response(models.Model):
  backup = models.ForeignKey(Backup, db_index=True)
  
  KEEP = 'KP'
  DEL  = 'DL'
  RESPONSE_CHOICES = (
      (KEEP, 'Keep data'),
      (DEL, 'Delete data'),
  )

  addedToWHDTicket = models.BooleanField(default=False)
  response = models.CharField(max_length=2, choices=RESPONSE_CHOICES, default=KEEP)
  justification = models.TextField(default="")
  created = models.DateTimeField(auto_now=False, auto_now_add=True)

  def __unicode__(self):
    return unicode(self.created)

class Restriction(models.Model):
  backup = models.ForeignKey(Backup, db_index=True)
  restrictContactsUntil = models.DateField(help_text="Input Format: MM/DD/YYYY")
  created = models.DateTimeField(auto_now=False, auto_now_add=True)
  updated = models.DateTimeField(auto_now=True, auto_now_add=False)

  def get_absolute_url(self):
    return reverse('BMS_App.views.restriction_view', kwargs={'pk' : self.pk})

class Email(models.Model):
  text = models.TextField();
  order = models.PositiveIntegerField()

  def __unicode__(self):
    return "Sent after " + unicode(self.order) + " contact(s). Text: " + unicode(self.text)


class IgnoreDirectory(models.Model):
  folderName = models.CharField(max_length=1024)
  
  def __unicode__(self):
    return unicode(self.folderName) 
