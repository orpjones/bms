from django.template import Library
register = Library()

@register.filter
def get_range( value ):
  """
  Filter - returns a list containing range made from given value
  Usage (in template):

  <ul>{% for i in 3|get_range %}
    <li>{{ i }}. Do something</li>
  {% endfor %}</ul>

  Results with the HTML:
  <ul>
  <li>0. Do something</li>
  <li>1. Do something</li>
  <li>2. Do something</li>
  </ul>

  Instead of 3 one may use the variable set in the views
  """
  return range( 1, value+1) # Range(x, y) goes from (x,y] so use y+1


@register.filter
def get_page_range( value , additional_pages):
  """
  Filter - returns a list containing range made from given
  value used for pagination. 

  additional_pages is how many pages on either side of
  current page will be displayed.

  Usage (in template):

  <ul>{% for i in 3|get_range 2 %}
    <li>{{ i }}. Do something</li>
  {% endfor %}</ul>

  Results with the HTML:
  <ul>
  <li>0. Do something</li>
  <li>1. Do something</li>
  <li>2. Do something</li>
  </ul>

  Instead of 3 one may use the variable set in the views
  """
  if(value == None):
    return range(0)
  if(additional_pages == None):
    return range( max(value - 3, 1), value + 3 + 1 )
  
  return range( max(value - additional_pages, 1), value + additional_pages + 1 )


@register.filter
def sub(value, amount):
  return value - amount


@register.filter
def convertToGB(value):
    return value / 1024
