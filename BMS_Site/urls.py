from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from django.conf import settings
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from BMS_App.views import BackupEditForm,CustomUpdateView, BackupIndexList, CustomCreateView, GenericCreatedTimestampView
from BMS_App.models import Client, Backup, Contact, Restriction, Response
from BMS_Site import settings

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'BMS_Site.views.home', name='home'),
    # url(r'^BMS_Site/', include('BMS_Site.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

     
##### Client URLs ###############
    # Index
      url(r'^clients/$', 'BMS_App.views.client_index', name='client_index'),
    # View
      url(r'^clients/(?P<pk>\d+)/view/$', 'BMS_App.views.client_view', name='client_view'),
    # Edit
      url(r'^clients/(?P<pk>\d+)/edit/$', 
        login_required(CustomUpdateView.as_view(
                        model=Client,
                        template_name="BMS_App/client_edit.html",
                        success_message="Client updated sucessfully")), 
        name='client_edit'),
      url(r'^clients/search/$', 'BMS_App.views.client_search', name='client_search'),


##### Backup URLs ###############
    # Index
      url(r'^backups/$', 'BMS_App.views.backup_index', name='backup_index'),
    # View
      url(r'^backups/(?P<pk>\d+)/view/$', 'BMS_App.views.backup_view', name='backup_view'),
    # Edit
      url(r'^backups/(?P<pk>\d+)/edit/$', 
        login_required(CustomUpdateView.as_view(
                        form_class=BackupEditForm,
                        model=Backup,
                        template_name="BMS_App/backup_edit.html",
                        success_message="Backup updated sucessfully")), 
        name='backup_edit'),
      url(r'^backups/([\w-]+)/$', BackupIndexList.as_view(), name='backup_index'),


##### Contact URLs ##############
    # Index
      url(r'^contacts/$', 'BMS_App.views.contact_index', name='contact_index'),
    # View
      url(r'^contacts/(?P<pk>\d+)/view/$', 'BMS_App.views.contact_view', name='contact_view'),
      url(r'^contacts/([\d+H|D|W|M|Y]+)/$', GenericCreatedTimestampView.as_view(), name='contact_index'),


##### Restriction URLs ##########
    # View
      url(r'^restrictions/(?P<pk>\d+)/view/$', 'BMS_App.views.restriction_view', name='restriction_view'),
    # Create  
      url(r'^restrictions/(?P<pk>\d+)/create/', 'BMS_App.views.restriction_create', name='restriction_create'),
    # Delete
      url(r'^restrictions/(?P<pk>\d+)/delete/$', 'BMS_App.views.restriction_delete', name='restriction_delete'),

##### Response URLs #############
    # Index
      url(r'^responses/$', 'BMS_App.views.response_index', name='response_index'),
    # View
      url(r'^responses/(?P<pk>\d+)/view/$', 'BMS_App.views.response_view', name='response_view'),
      url(r'^responses/([\d+H|D|W|M|Y]+)/$', GenericCreatedTimestampView.as_view(template_name="BMS_App/response_index.html", model=Response), name='response_index'),

##### Home Page #################
    url(r'^$', 'BMS_App.views.home', name='home'),
#################################

    # Login, logout
    url(r'^login/$',  'django.contrib.auth.views.login', 
        {'template_name' : 'login.html'}, name='tech_login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page' : settings.LOGIN_REDIRECT_URL}, name='tech_logout'),

    # Password change
    url(r'^user/password/change/$', 'django.contrib.auth.views.password_change', 
        {'template_name' : 'password_change.html'}, name='password_change'),
    
    url(r'^user/password/change/done$', 'django.contrib.auth.views.password_change_done', 
        {'template_name' : 'password_change_done.html'}, name='password_change_done'),
    # Password reset
    url(r'^user/password/reset/$', 'django.contrib.auth.views.password_reset', 
        {'post_reset_redirect' : '/user/password/reset/done/', 
          'template_name' : 'registration/password_reset.html'}, 
        name="password_reset"),
    url(r'^user/password/reset/done/$', 'django.contrib.auth.views.password_reset_done'),
    url(r'^user/password/reset/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$',
          'django.contrib.auth.views.password_reset_confirm', 
          {'post_reset_redirect' : '/user/password/done/'}),
    url(r'^user/password/done/$','django.contrib.auth.views.password_reset_complete'),

    # Wiki
    url(r'^wiki/$', login_required(TemplateView.as_view(template_name="wiki.html")), name="wiki"),
)
