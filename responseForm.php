<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Backup Management System</title>
    <meta name="description" content="ABNS">
    <meta name="author" content="Oliver Jones">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/assets/css/bootstrap.css">
    <link rel="stylesheet" href="/assets/css/bootstrap-responsive.css">
	
	<style type="text/css">
		.error {color:red;}
	</style>
  </head>
  
  <script>
	function validateRadio (radios)
	{
		for (i = 0; i < radios.length; ++ i)
		{
			if (radios [i].checked) return true;
		}
		return false;
	}

	function validateForm()
	{
		var radios = document.forms["responseForm"]["radio"];
		var valid = false;
		var keep = false;
		for (i = 0; i < radios.length; ++ i)
		{
			if (radios [i].checked)
			{
				if(radios[i].value == "No")
					keep = true;
				valid = true;
				break;
			}
		}

		if (!valid)
		{
		  alert("You must make a selection.");
		  return false;
		}
		
		if(keep)
		{
			var textarea = document.forms["responseForm"]["description"].value;
			if (textarea == null || textarea == "")
			{
				alert("Please enter a brief description of why you need us to hang onto your data.");
				return false;
			}
		}
	}
  </script>

  <body>
	<div class="container" id="mainContainer">
	<h2>STS Backup Response Form</h2>
	
	
	
  <?php
		#Debug stuff
		ini_set('display_errors', 'On');
		error_reporting(E_ALL);
		
		date_default_timezone_set("UTC");
		$errorMessage = "<div class=\"alert alert-error\">We're sorry, something went wrong. Please try again or call STS at (314) 935-7100.</div>";
		
		$username = "root";
		$password = "@";
		$hostname = ".."; 
		
		$showForm    = False;
		$showError   = False;
		$showSuccess = False;
		
		//connection to the database
		$link = mysql_connect($hostname, $username, $password) or die($errorMessage . mysql_error());
		mysql_select_db("bms_db") or die($errorMessage . mysql_error());
		
		if(isset($_GET["id"]))
			$backup_hash = mysql_real_escape_string($_GET["id"]);
		else if(isset($_POST["id"]))
			$backup_hash = mysql_real_escape_string($_POST["id"]);
		else die($errorMessage . "<div class=\"error\">Error: Missing ID</div>");
		
		$query = "SELECT * FROM BMS_App_backup WHERE hash_id='{$backup_hash}'";
		$result = mysql_query($query);
		
		if (!$result || mysql_num_rows($result) == 0) {
			die($errorMessage . mysql_error());
		}
		
		$row = mysql_fetch_assoc($result);
		$backup_id = mysql_real_escape_string($row["id"]);
                $ticket_number = $row["ticket_number"];

		// The form has been submitted so process the response. Otherwise, show the form.
		if(isset($_POST["id"]))
		{
			if(!isset($_POST["radio"])) die($errorMessage . "<div class=\"error\">Error: Missing POST Values</div>");
			$choice = $_POST["radio"];

			$query = "";
			$createdTimestamp = date ("Y-m-d H:i:s", time());

			// They want us to delete their data, so create the response and then change the status of the backup.
			if(strcmp($choice, "Yes") == 0) 
			{ 
				$choice = "DL"; 
				$query = "INSERT INTO BMS_App_response (backup_id, response, created) VALUES ('{$backup_id}', '{$choice}', '{$createdTimestamp}')";
				$updateQuery = "UPDATE BMS_App_backup SET status='CLO' WHERE id='{$backup_id}'";
				$showSuccess = mysql_query($query) && mysql_query($updateQuery);
                                $responseText = "Client responded on " . $createdTimestamp . ". Wants us to delete data. Marked backup appropriately.";
                        }
  			else // They want us to keep their data, so create the response and then create a restriction.
			{
				$choice = "KP";
				if(!isset($_POST["description"])) die($errorMessage . "<div class=\"error\">Error: Missing POST Values</div>");
				$description = $_POST["description"];
				$query = "INSERT INTO BMS_App_response (backup_id, response, justification, created) VALUES ('{$backup_id}', '{$choice}', '{$description}', '{$createdTimestamp}')";
				
				$now = time() + (7 * 24 * 60 * 60); // Add on a week from now.
				$restrictContactsUntil = date ("Y-m-d", $now);
				$insertRestrictionQuery = "INSERT INTO BMS_App_restriction (backup_id, restrictContactsUntil, created, updated) VALUES ('{$backup_id}', '{$restrictContactsUntil}', '{$createdTimestamp}', 'NOW()')";
				$showSuccess = mysql_query($query) && mysql_query($insertRestrictionQuery);
                                $responseText = "Client responded on " . $createdTimestamp . ". Wants us to keep data. Will resume emailing in 1 week. Justification: " . $description;
			}
			
			if(!$showSuccess)
			{
				die($errorMessage . "<div class=\"error\">Error: " . mysql_error() . "</div>");
			}
			
		}
		else
		{			
			// Just show the form. Unless more than the maximum amount of days has passed. 35 at the time of this writing. (6/3/13)
			$created = strtotime($row["created"]);
			
			// These are the number of contacts/number of responses. Originally, was going to use these to calculate if
			// they can respond. Ended up just doing it by number of days. Not currently used, but may be used in the future.
			$contactsQuery = "SELECT * FROM BMS_App_contact WHERE backup_id='{$backup_id}'";
			$contactsResult = mysql_query($contactsQuery);
			$numContacts = mysql_num_rows($contactsResult);
			
			$responsesQuery = "SELECT * FROM BMS_App_response WHERE backup_id='{$backup_id}'";
			$responsesResult = mysql_query($responsesQuery);
			$numResponses = mysql_num_rows($responsesResult);

			if (!$contactsResult || !$responsesResult) {
				die($errorMessage);
			}
			
			$now = time();
			$diff = $now - $created;
			$diffDays = $diff/(60 * 60 * 24);
			$MAX_DAYS_ELAPSED = 35;
			$showForm = ($diffDays < $MAX_DAYS_ELAPSED);
			$showError = !$showForm;
			
			mysql_free_result($contactsResult);
			mysql_free_result($responsesResult);
		}
		
		mysql_free_result($result);
		mysql_close($link);
  ?>
  
  
		<?php if($showForm) : ?>
		<form id="responseForm" action="responseForm.php" method="post" class="form-horizontal" onsubmit="return validateForm()">
			<input type="hidden" name="id" value="<?php echo $_GET["id"]; ?>" />
			<div class="control-group">
			    <label class="control-label" for="response">Can we delete your data?</label>
				<div class="controls">
					<label class="radio">
						<input type="radio" name="radio" onchange="document.getElementById('description').style.display='none'" id="radio1_Yes" value="Yes">
						Yes:
					</label>	
					<label class="radio">
						<input type="radio" name="radio" onchange="document.getElementById('description').style.display='inline'" id="radio1_No" value="No">		
						No:
					</label>
				</div>
			</div>
			<div class="control-group" id="description" style="display:none;">
				<label class="control-label" for="descriptiontext">Please enter a brief description of why you need us to keep your data:</label>
					<div class="controls">
					  <textarea name="description" rows="10"></textarea>
					</div>				
			</div>
			<div class="control-group">
				<div class="controls">
					<button type="submit" class="btn">Submit</button>
				</div>
			</div>
		</form>
		<?php elseif ($showError): ?>
			<div class="alert alert-error">
				<p>Unfortunately, too much time has elapsed and your data has been scheduled for deletion. If you believe 
				this to be a mistake, please call STS immediately at <strong>(314) 935-7100.</strong></p>
			</div>
                <?php elseif ($showSuccess): ?>
                        <div class="alert alert-success">
				<p>Thank you, your response has been recorded.</p>
			</div>
		<?php endif; ?>

	</div>
  </body>
  
  
  <footer class="row">
	  <hr size="2" COLOR="black" />
	  <div class="span12" style="text-align:right">
		<p>
		&copy; <a href="http://sts.wustl.edu">Student Technology Services</a>
		</p>
	  </div>
  </footer>
</html>
